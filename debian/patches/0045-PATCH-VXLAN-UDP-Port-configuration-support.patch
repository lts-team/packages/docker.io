From: selansen <elango.siva@docker.com>
Date: Fri, 26 Oct 2018 22:51:34 -0400
Subject: [PATCH] VXLAN UDP Port configuration support

This PR chnages allow user to configure VxLAN UDP
port number. By default we use 4789 port number. But this commit
will allow user to configure port number during swarm init.
VxLAN port can't be modified after swarm init.

This is a depends of fixing CVE-2023-2884[0-2]

Signed-off-by: selansen <elango.siva@docker.com>
origin: https://github.com/moby/moby/commit/077ccabc4533d8d0ae13cd009e9c91a111d55e62.patch
---
 libnetwork/drivers/overlay/encryption.go         |  5 +--
 libnetwork/drivers/overlay/ov_utils.go           |  3 +-
 libnetwork/drivers/overlay/overlay.go            |  1 -
 libnetwork/drivers/overlay/overlayutils/utils.go | 44 ++++++++++++++++++++++++
 4 files changed, 49 insertions(+), 4 deletions(-)
 create mode 100644 libnetwork/drivers/overlay/overlayutils/utils.go

diff --git a/libnetwork/drivers/overlay/encryption.go b/libnetwork/drivers/overlay/encryption.go
index a97e73d..38fd710 100644
--- a/libnetwork/drivers/overlay/encryption.go
+++ b/libnetwork/drivers/overlay/encryption.go
@@ -12,6 +12,7 @@ import (
 
 	"strconv"
 
+	"github.com/docker/libnetwork/drivers/overlay/overlayutils"
 	"github.com/docker/libnetwork/iptables"
 	"github.com/docker/libnetwork/ns"
 	"github.com/docker/libnetwork/types"
@@ -200,7 +201,7 @@ func removeEncryption(localIP, remoteIP net.IP, em *encrMap) error {
 
 func programMangle(vni uint32, add bool) (err error) {
 	var (
-		p      = strconv.FormatUint(uint64(vxlanPort), 10)
+		p      = strconv.FormatUint(uint64(overlayutils.GetVxlanUDPPort()), 10)
 		c      = fmt.Sprintf("0>>22&0x3C@12&0xFFFFFF00=%d", int(vni)<<8)
 		m      = strconv.FormatUint(uint64(r), 10)
 		chain  = "OUTPUT"
@@ -227,7 +228,7 @@ func programMangle(vni uint32, add bool) (err error) {
 
 func programInput(vni uint32, add bool) (err error) {
 	var (
-		port       = strconv.FormatUint(uint64(vxlanPort), 10)
+		port       = strconv.FormatUint(uint64(overlayutils.GetVxlanUDPPort()), 10)
 		vniMatch   = fmt.Sprintf("0>>22&0x3C@12&0xFFFFFF00=%d", int(vni)<<8)
 		plainVxlan = []string{"-p", "udp", "--dport", port, "-m", "u32", "--u32", vniMatch, "-j"}
 		ipsecVxlan = append([]string{"-m", "policy", "--dir", "in", "--pol", "ipsec"}, plainVxlan...)
diff --git a/libnetwork/drivers/overlay/ov_utils.go b/libnetwork/drivers/overlay/ov_utils.go
index 27f57c1..69e691d 100644
--- a/libnetwork/drivers/overlay/ov_utils.go
+++ b/libnetwork/drivers/overlay/ov_utils.go
@@ -5,6 +5,7 @@ import (
 	"strings"
 	"syscall"
 
+	"github.com/docker/libnetwork/drivers/overlay/overlayutils"
 	"github.com/docker/libnetwork/netutils"
 	"github.com/docker/libnetwork/ns"
 	"github.com/docker/libnetwork/osl"
@@ -61,7 +62,7 @@ func createVxlan(name string, vni uint32, mtu int) error {
 		LinkAttrs: netlink.LinkAttrs{Name: name, MTU: mtu},
 		VxlanId:   int(vni),
 		Learning:  true,
-		Port:      vxlanPort,
+		Port:      int(overlayutils.GetVxlanUDPPort()),
 		Proxy:     true,
 		L3miss:    true,
 		L2miss:    true,
diff --git a/libnetwork/drivers/overlay/overlay.go b/libnetwork/drivers/overlay/overlay.go
index 7045888..6ec58df 100644
--- a/libnetwork/drivers/overlay/overlay.go
+++ b/libnetwork/drivers/overlay/overlay.go
@@ -25,7 +25,6 @@ const (
 	vethLen      = 7
 	vxlanIDStart = 256
 	vxlanIDEnd   = (1 << 24) - 1
-	vxlanPort    = 4789
 	vxlanEncap   = 50
 	secureOption = "encrypted"
 )
diff --git a/libnetwork/drivers/overlay/overlayutils/utils.go b/libnetwork/drivers/overlay/overlayutils/utils.go
new file mode 100644
index 0000000..a2a7387
--- /dev/null
+++ b/libnetwork/drivers/overlay/overlayutils/utils.go
@@ -0,0 +1,44 @@
+// Package overlayutils provides utility functions for overlay networks
+package overlayutils
+
+import (
+	"fmt"
+	"sync"
+)
+
+var (
+	vxlanUDPPort uint32
+	mutex        sync.Mutex
+)
+
+func init() {
+	vxlanUDPPort = 4789
+}
+
+// ConfigVxlanUDPPort configures vxlan udp port number.
+func ConfigVxlanUDPPort(vxlanPort uint32) error {
+	mutex.Lock()
+	defer mutex.Unlock()
+	// if the value comes as 0 by any reason we set it to default value 4789
+	if vxlanPort == 0 {
+		vxlanPort = 4789
+	}
+	// IANA procedures for each range in detail
+	// The Well Known Ports, aka the System Ports, from 0-1023
+	// The Registered Ports, aka the User Ports, from 1024-49151
+	// The Dynamic Ports, aka the Private Ports, from 49152-65535
+	// So we can allow range between 1024 to 49151
+	if vxlanPort < 1024 || vxlanPort > 49151 {
+		return fmt.Errorf("ConfigVxlanUDPPort Vxlan UDP port number is not in valid range %d", vxlanPort)
+	}
+	vxlanUDPPort = vxlanPort
+
+	return nil
+}
+
+// GetVxlanUDPPort returns Vxlan UDP port number
+func GetVxlanUDPPort() uint32 {
+	mutex.Lock()
+	defer mutex.Unlock()
+	return vxlanUDPPort
+}
